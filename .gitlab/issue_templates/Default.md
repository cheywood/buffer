<!--
Thanks for taking the time to submit an issue!

Bug submitters: Flatpak is the supported distribution of Buffer. If you normally run with a package from another source, checking that you can reproduce the issue with the latest build from Flathub would be great.

Feature requesters: One way of describing Buffer is as an answer to the question "what's the least that can be wrapped around a multi-line text widget and still be useful?". So the aim here is to do less. Inherently that makes the scrope for feature requests a bit limited! But you're very welcome to submit them :)
-->

### Bug environment
<!-- ignore/delete for feature requests -->

- Buffer version: 
- Desktop environment: 
- Package source: <!-- eg. Flathub or Linux distribution -->
- Distribution: 


