# Changelog

All notable changes will be documented in this file, the format for which is based on [Keep a Changelog](https://keepachangelog.com/). Higher level changes are tracked in the [AppStream](https://gitlab.gnome.org/cheywood/buffer/-/blob/main/data/org.gnome.gitlab.cheywood.Buffer.metainfo.xml.in.in).

## [0.9.9] - 2025-02-24

### Added

- Help text for moving the window
- Flatpak: Patch libspelling to remove false positives for contractions in English

### Changed

- Switch to target Python v3.11+
- Brazilian Portuguese translation update by Filipe Motta (@Tuba2)
- German translation update by Jürgen Benvenuti (@gastornis)
- Italian translation update by Sebastiano D'Angelo (@nostalgickitsune)


## [0.9.8] - 2025-02-07

### Added

- Italian translation by Sebastiano D'Angelo (@nostalgickitsune)


## [0.9.7] - 2024-10-15

### Added

- Brazilian Portuguese translation by Filipe Motta (@Tuba2)
- Norwegian Bokmål translation by sunniva (@turtlegarden)

### Changed

- German translation update by Jürgen Benvenuti (@gastornis)

### Fixed

- Spell checking


## [0.9.6] - 2024-09-18

### Added

- Margin maintained below cursor when appending at bottom of window
- `F10` menu shortcut

### Changed

- Switch to v47 SDK
- Switch to target Python v3.9+
- Update CSS for GTK v4.16 variables and deprecations
- Tweak strings for capitalisation consistency
- German translation update by Jürgen Benvenuti (@gastornis)

### Fixed

- `ESC` not closing preferences


## [0.9.5] - 2024-08-08

### Fixed

- `Ctrl + F` not refocusing the search entry while searching
- Poor interaction editing while searching


## [0.9.4] - 2024-08-05

### Added

- Improved visual separation between the buttons and text for small windows (#6)


## [0.9.3] - 2024-08-04

### Added

- Improved visual separation between the buttons and text for small windows
- Translator comments to AppStream screenshot captions by Rafael Fontenelle (@rafaelff)

### Fixed

- Window not having a minimum width by Vlad Krupinski (@mrvladus)
- Right clicking impacting the selection (#5)
- Creating a buffer with clipboard contents (from CLI)


## [0.9.2] - 2024-08-03

### Added

- The ability to drag the window via the button area
- German translation by Jürgen Benvenuti (@gastornis)


## [0.9.1] - 2024-04-19

### Added

- Handling of `SIGINT` and `SIGTERM` signals so that when received the (optional) emergency saves are
  created before closing
- Ability to show line numbers
- `Ctrl + Shift + L` shortcut to toggle line length limit

### Changed

- Reduce minimum margin for thin windows
- Tweak preferences help text

### Fixed

- `ESC` not closing preferences
- Line length limit auto-disabling when reaching maximum not accounting for margins
- Jumps in margin values


## [0.9.0] - 2024-04-09

### Added

- Initial release
